/**
 * This is our Endless Expert React/Redux Application. Here, we setup the bindings between the props, state, and actions.
 *
 * This class should contain nearly 100% of the actual references to state and actions. We pass that information down
 * to child components via props.
 *
 * This application uses a presence session (see {@link https://tokbox.com/developer/sdks/js/reference/Session.html}) -
 * a global session which every client connects to in order to communicate presence information. No video streaming is
 * done in this session. Once we establish the presence session, this component listens to and reacts to that session
 * in order to communicate to those attached to the session who is present and available for chatting.
 */
import React                  from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import * as UserActions from '../actions/users';
import * as PresenceActions from '../actions/presence';
import * as InvitationActions from '../actions/invitations';

import { MainView } from '../views/MainView';

// We define mapStateToProps and mapDispatchToProps where we'd normally use
// the @connect decorator so the data requirements are clear upfront, but then
// export the decorated component after the main class definition so
// the component can be tested w/ and w/o being connected.
// See: http://rackt.github.io/redux/docs/recipes/WritingTests.html
const mapStateToProps = (state) => ({
  localUser: state.localUser,
  remoteUsers: state.remoteUsers,
  presence: state.presence,
  invitations: state.invitations,
  routerState: state.router
});

const actions = {};

Object.assign(actions, UserActions, PresenceActions, InvitationActions);

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
});

export class App extends React.Component {
  static propTypes = {
    actions: React.PropTypes.object.isRequired,
    localUser: React.PropTypes.object.isRequired,
    remoteUsers: React.PropTypes.array.isRequired,
    presence: React.PropTypes.object.isRequired,
    invitations: React.PropTypes.array.isRequired
  }

  componentWillMount () {
    // initialize our presence session and setup basic event handlers
    this.props.actions.initPresenceSession((presenceSession) => {
      presenceSession.on('connectionCreated', this.presenceConnectionCreated, this);
      presenceSession.on('connectionDestroyed', this.presenceConnectionDestroyed, this);
      presenceSession.on('signal:invitation', this.invitationReceived, this);
      presenceSession.on('signal:cancelOrDeclineInvitation', this.invitationCanceledOrDeclined, this);
    });
  }

  /**
   * Handles the creation of the presence session connection. If this connection is not the local user (that user logged
   * into this application), add the user as a "Remote User" that can be chatted with.
   *
   * @param {Object} connectionEvent - The connection created event {@link https://tokbox.com/developer/sdks/js/reference/ConnectionEvent.html}
   */
  presenceConnectionCreated (connectionEvent) {
    /* the local user connects, too ... only add remote users */
    if (this.props.localUser.connection.connectionId !== connectionEvent.connection.connectionId) {
      this.props.actions.addRemoteUser(connectionEvent.connection);
    }
  }

  /**
   * Handles receiving an invitation on the Presence Session to chat, by adding that invitation.
   *
   * @param {Object} signalEvent - The event signaling an invitation has been received
   *  {@link https://tokbox.com/developer/sdks/js/reference/SignalEvent.html}
   */
  invitationReceived (signalEvent) {
    this.props.actions.addInvitation(signalEvent);
  }

  /**
   * Handles receiving a cancel or decline event by removing that invitation.
   *
   * @param {Object} signalEvent - The event signaling an invitation has been canceled or declined
   *  {@link https://tokbox.com/developer/sdks/js/reference/SignalEvent.html}
   */
  invitationCanceledOrDeclined (signalEvent) {
    this.props.actions.removeInvitation(signalEvent);
  }

  /**
   * Handles cleanup once a connection to the Presence Session has been destroyed (i.e. a Remote User has logged out).
   *
   * @param {Object} signalEvent - The event signaling a presence session connection has been destroyed {@link
    * https://tokbox.com/developer/sdks/js/reference/ConnectionEvent.html}
   */
  presenceConnectionDestroyed (signalEvent) {
    if (signalEvent.connection.connectionId !== this.props.presence.session.connection.connectionId) {
      this.props.actions.removeRemoteUser(signalEvent.connection);
    }
  }

  /**
   * React Component Renderer
   *
   * @returns {*} Either Null if there is no Presence Session or the Endless Expert App.
   */
  render () {
    if (this.props.presence.session) {
      return (
        <MainView {...this.props} />
      );
    } else {
      return null;
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

/**
 * These are the Chat Invitation actions.
 */
import * as types from '../constants/ActionTypes';
import api from '../api';

/**
 * Action for adding an invitation. The action only passes through the dispatch to the invitation reducer, which
 * adds the invitation to the store.
 *
 * @param {Object} invitation - The invitation to add.
 * @returns {Function} The dispatch function for adding the invitation.
 */
export function addInvitation(invitation) {
  return (dispatch) => {
    dispatch({
      type: types.ADD_INVITATION, invitation
    });
  };
}

/**
 * Action for inviting a remote user to chat. This action leverages our API, which proxies this request to the
 * OpenTok presence session. The action then dispatches the invitation to the reducer so that it is added to store.
 *
 * @param {Object} remoteUser - The user being invited.
 * @param {Object} localUser - The inviter.
 * @param {Object} presence - The OpenTok Presence Session used for communication of the invitation.
 * @returns {Function} The dispatch function for adding the remote invitation.
 */
export function inviteRemoteUser(remoteUser, presence) {
  return (dispatch) => {
    api.inviteRemoteUser(remoteUser, presence);
    dispatch({
      type: types.INVITE_REMOTE_USER, remoteUser
    });
  };
}

/**
 * Action for removing an invitation. The action only passes through the dispatch to the invitation reducer, which
 * removes the invitation to the store.
 *
 * @param {Object} invitation - The invitation to remove.
 * @returns {Function} The dispatch function for adding the invitation.
 */
export function removeInvitation(invitation) {
  return (dispatch) => {
    dispatch({
      type: types.REMOVE_INVITATION, invitation
    });
  };
}

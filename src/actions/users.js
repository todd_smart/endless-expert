/**
 * These are the User actions.
 */
import * as types from '../constants/ActionTypes';
import api from '../api';

/**
 * This action is used for adding the local user to the Presence Session (via the API) as well as dispatching a
 * request to the users reducer to add the local user to our store.
 *
 * @param {String} name - The name of the local user. Note that we don't use this name to identify users. We use their
 * Presence Session connection ID.
 * @param {Object} presence - A reference to the Presence Session for use within the API.
 * @returns {Function} The dispatch function for communicating the request to add the local user to the store.
 */
export function setLocalUser(name, presence) {
  const user = {
    name : name
  };
  return (dispatch) => {
    api.addUser(user, presence, () => {
      user.connection = presence.session.connection;
      dispatch({
        type: types.SET_LOCAL_USER, user
      });
    });
  };
}

/**
 * This action is used to add the remote user to the store.
 *
 * @param {Object} connection - The {@link https://tokbox.com/developer/sdks/js/reference/Connection.html} of the
 * remote user.
 * @returns {Function} The dispatch function for communicating the request to add the remote user to the store.
 */
export function addRemoteUser(connection) {
  return (dispatch) => {
    const user = JSON.parse(connection.data);
    user.connection = connection;
    dispatch({
      type: types.ADD_REMOTE_USER, user
    });
  };
}

/**
 * This action is used to remove the remote user to the store.
 *
 * @param {Object} connection - The {@link https://tokbox.com/developer/sdks/js/reference/Connection.html} of the
 * remote user.
 * @returns {Function} The dispatch function for communicating the request to add the remote user to the store.
 */
export function removeRemoteUser(connection) {
  return (dispatch) => {
    const user = JSON.parse(connection.data);
    user.connection = connection;
    dispatch({
      type: types.REMOVE_REMOTE_USER, user
    });
  };
}

/**
 * These are the OpenToke Presence Session actions.
 */
import * as types from '../constants/ActionTypes';
import api from '../api';

/**
 * This action leverages the API to initialize the presence session, which essentially returns the presence session
 * that we refer to in our store.
 *
 * Note that we use this presence session for signaling users of who is available to chat and for
 * making chat requests.
 *
 * @param {Function} callback - We return the presence session to the app for ease of access in addition to adding it
 * to the store.
 * @returns {Function} The dispatch for the presence reducer to add the presence session to the store.
 */
export function initPresenceSession(callback) {
  return (dispatch) => {
    api.initPresenceSession((presenceSession) => {
      dispatch({
        type: types.INIT_PRESENCE_SESSION, presenceSession
      });
      callback(presenceSession);
    });
  };
}

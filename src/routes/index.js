/**
 * This script was provided by the {@link https://github.com/davezuko/react-redux-starter-kit.}
 *
 * Our modification was to add the {@link /container/App.js} entry point.
 */
import { Route, IndexRoute }   from 'react-router';
import React       from 'react';
import CoreLayout  from 'layouts/CoreLayout';
import App         from 'containers/App';

export default (
  <Route path='/' component={CoreLayout}>
    <IndexRoute component={App} />
  </Route>
);

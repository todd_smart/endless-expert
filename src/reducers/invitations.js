/**
 * Invitation Reducers.
 */
import { ADD_INVITATION, INVITE_REMOTE_USER, REMOVE_INVITATION } from '../constants/ActionTypes';

const initialState = [];

export default function invitations(state = initialState, action = null) {
  switch (action.type) {

  // add an invitation
  case ADD_INVITATION:
    const data = JSON.parse(action.invitation.data);
    return [{
      id: state.reduce((maxId, invitation) => Math.max(invitation.id, maxId), -1) + 1,
      incoming: true,
      remoteUser: action.invitation.from,
      sessionId: data.sessionId,
      token: data.token,
      apiKey: data.apiKey
    }, ...state];

  // invite a remote user to an invitation
  case INVITE_REMOTE_USER:
    return [{
      id: state.reduce((maxId, invitation) => Math.max(invitation.id, maxId), -1) + 1,
      incoming: false,
      remoteUser: action.remoteUser.connection
    }, ...state];

  // remove an invitation
  case REMOVE_INVITATION:
    const invitationIndex = state.findIndex(invitation => invitation.remoteUser.connectionId === action.invitation.from.connectionId);
    return state.splice(0, invitationIndex);

  default:
    return state;
  }
}

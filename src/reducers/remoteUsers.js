/**
 * Remote User reducers - used for keeping up with who else is attached to the Presence Session
 */
import { ADD_REMOTE_USER, REMOVE_REMOTE_USER } from '../constants/ActionTypes';

const initialState = [];

export default function remoteUsers(state = initialState, action = null) {
  switch (action.type) {

  // add a remote user
  case ADD_REMOTE_USER:
    const user = action.user;
    user.id = state.reduce((maxId, thisUser) => Math.max(thisUser.id, maxId), -1) + 1;
    return [user, ...state];

  // remove a remote user
  case REMOVE_REMOTE_USER:
    const userIndex = state.findIndex(thisUser => thisUser.connection.connectionId === action.user.connection.connectionId);
    return state.splice(0, userIndex);

  default:
    return state;
  }
}

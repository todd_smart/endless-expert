import { combineReducers } from 'redux';
import localUser from './localUser';
import remoteUsers from './remoteUsers';
import presence from './presence';
import invitations from './invitations';
import { routerStateReducer } from 'redux-router';

export default combineReducers({
  localUser,
  remoteUsers,
  presence,
  invitations,
  router: routerStateReducer
});

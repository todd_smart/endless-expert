/**
 * Local User reducers.
 */
import { SET_LOCAL_USER } from '../constants/ActionTypes';

const initialState = {};

export default function localUser(state = initialState, action = null) {
  switch (action.type) {

  // set the local (logged in) user
  case SET_LOCAL_USER:
    return action.user;

  default:
    return state;
  }
}

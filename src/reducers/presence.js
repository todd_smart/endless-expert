/**
 * Presence Session reducer. We generally keep this in the store just for ease of access within the application state.
 */
import { RETRIEVE_PRESENCE_CONFIG, INIT_PRESENCE_SESSION } from '../constants/ActionTypes';

export default function presence(state = {}, action = null) {
  switch (action.type) {

  // Get the presence session config
  case RETRIEVE_PRESENCE_CONFIG:
    return state;

  // Set the presence session config
  case INIT_PRESENCE_SESSION:
    return {
      session: action.presenceSession
    };
  default:
    return state;
  }
}

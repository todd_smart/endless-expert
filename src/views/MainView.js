/**
 * This is the main view of the Endless Expert. It is responsible for including the Site Navigation, the List of Experts,
 * the List of Invitations, the Connection (login) Modal, and the User Info.
 */
import React from 'react';

import { Row, Col, Nav, Navbar, NavBrand } from 'react-bootstrap';

import { UserInfo } from '../components/UserInfo';
import { ConnectModal } from '../components/ConnectModal';
import { ExpertList } from '../components/ExpertList';
import { InvitationList } from '../components/InvitationList';
import { VideoHolder } from '../components/VideoHolder';

export class MainView extends React.Component {
  static propTypes = {
    presence: React.PropTypes.object.isRequired,
    actions : React.PropTypes.object.isRequired,
    localUser: React.PropTypes.object.isRequired,
    remoteUsers: React.PropTypes.array.isRequired,
    invitations: React.PropTypes.array.isRequired
  }

  /**
   * The React Component Renderer
   *
   * @returns {XML}
   */
  render () {
    return (
      <div>
        <Row>
          <Col sm={8}>
            <Navbar>
              <NavBrand><a href='#'>Endless Expert: Chat with Presence</a></NavBrand>
              <Nav navbar right>
                <UserInfo localUser={this.props.localUser} />
                <ConnectModal presence={this.props.presence} setLocalUser={this.props.actions.setLocalUser} show={!this.props.localUser.name} />
              </Nav>
            </Navbar>
            <Row>
              <InvitationList presence={this.props.presence} invitations={this.props.invitations} localUser={this.props.localUser} />
            </Row>
            <Row>
              <Col sm={4}>
                <ExpertList presence={this.props.presence} remoteUsers={this.props.remoteUsers} inviteRemoteUser={this.props.actions.inviteRemoteUser} />
              </Col>
              <Col sm={8}>
                <VideoHolder presence={this.props.presence} inviteRemoteUser={this.props.actions.inviteRemoteUser} removeInvitation={this.props.actions.removeInvitation}/>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

/**
 * This API provides the facade and implementation for all client-server ajax processing. It also communicates with
 * the OpenTok Client-side library {@link https://tokbox.com/developer/sdks/js/} where we found that communication more
 * appropriate than within our other UI classes.
 */
/* global OT {@link https://tokbox.com/developer/sdks/js/reference/OT.html} */
import request from 'superagent';

export default {

  /**
   * Initializes our OpenTok Presence Session (See references at {@link https://github.com/opentok/presencekit-php} and
   * {@link https://tokbox.com/developer/sdks/js/reference/Session.html}.
   *
   * NOTE: The OpenTok Presence Session is 'supposed to be' available at all times. Our service call simply reaches
   * out to the OpenTok account associated with the Endless Expert service to retrieve the sesion details, such as its
   * API KEY and SESSION ID.
   *
   * @param {function} callback - A callback function clients can use to be notified of the availability of the
   * presence session.
   */
  initPresenceSession: function initPresenceSession (callback) {
    request
      .get('/services/presence')
      .end(function processResponse(err, res) {
        const presenceSession = OT.initSession(res.body.apiKey, res.body.sessionId);
        callback(presenceSession);
      });
  },

  /**
   * Adds users to a given OpenTok presence session. See references at
   * {@link https://github.com/opentok/presencekit-php} and
   * {@link https://tokbox.com/developer/sdks/js/reference/Session.html}.
   *
   * NOTE: Once a user is added to this presence session, all others connected to that session can observe their
   * connectivity and communicate with them through the session (i.e. to request a separate chat session).
   *
   * @param {Object} user - The user to add to the OpenTok Presence Session
   * @param {Object} presence - The OpenTok Presence Session
   *  {@Link https://tokbox.com/developer/sdks/js/reference/Session.html}
   * @param {function} callback - A callback function clients can use to be notified of the addition of the user
   *  to the OpenTok Presence Session.
   */
  addUser: function addUser (user, presence, callback) {
    request
      .post('/services/users')
      .send({'name' : user.name})
      .end(function processResponse(err, res) {
        user.token = res.body.token;
        presence.session.connect(user.token, () => {
          callback();
        });
      });
  },

  /**
   * Used by the local user in one UI browser session to invite a remote user in another UI browser session to a chat.
   * This request is made through their mutual association to the OpenTok Presence Session. See references at
   * {@link https://github.com/opentok/presencekit-php} and
   * {@link https://tokbox.com/developer/sdks/js/reference/Session.html}.
   *
   * @param {Object} remoteUser - A remote user to invite to a chat. This user is identified through their
   *  OpenTok Presence Session Connection {@link https://tokbox.com/developer/sdks/js/reference/Connection.html}.
   * @param {Object} presence - The OpenTok Presence Session
   *  {@Link https://tokbox.com/developer/sdks/js/reference/Session.html} used to invite the also connected remote guest
   */
  inviteRemoteUser: function inviteRemoteUser (remoteUser, presence) {
    /* Here, we need to get a new chat session from the server - THEN signal */
    request
      .post('/services/chats')
      .send()
      .end(function processResponse(err, res) {
        const sessionData = res.body;
        const signal = {
          type: 'invitation',
          to: remoteUser.connection,
          data: JSON.stringify({
            sessionId: sessionData.sessionId,
            apiKey: sessionData.apiKey,
            token: sessionData.token
          })
        };
        presence.session.signal(signal);
      });
  }

};

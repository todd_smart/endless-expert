/**
 * The ConnectModal is responsible for gathering the user details required to connect to the presence session,
 * and for connecting to the presence session by leveraging the
 * {@link /src/actions/users} setLocalUser action to add the LocalUser to the {@link /src/stores/index}.
 */
import React from 'react';
import { Modal, Input, Button } from 'react-bootstrap';

export class ConnectModal extends React.Component {

  static propTypes = {
    setLocalUser: React.PropTypes.func.isRequired,
    show: React.PropTypes.bool.isRequired,
    presence: React.PropTypes.object.isRequired
  }

  /**
   * This event handler method is required by React for all modals. It does nothing special in this instance.
   */
  onHide () {
  }

  /**
   * Click handler for user to set the local user.
   */
  handleClick () {
    const node = this.refs.name;
    const name = node.getValue().trim();
    this.props.setLocalUser(name, this.props.presence);
  }

  /**
   * Render the component.
   *
   * @returns {XML}
   */
  render () {
    return (
        <Modal show={this.props.show} container={this} aria-labelledby='contained-modal-title' onHide={this.onHide}>
        <Modal.Header>
          <Modal.Title id='contained-modal-title-lg'>Welcome to Endless Expert: Chat with Presence</Modal.Title>
          <Modal.Title id='contained-modal-title-sm'>An OpenTok one-to-one solution focused on adding presence</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>If this is your first time testing the application, you will need a group of users to also join and test with you. If you would like
            to start testing on your own, you could also open <a href='/main' target='_blank'>another window with the same address</a>, side by side,
            and enter two different names.</p>
          <p>After pressing the 'Connect' button below, the upper right shows your username and indicates that you are connected. All other users
            that are connected will appear in the 'Experts' list. When a user is available for a call, the camera icon button next to their name
            is used to invite that user to a video chat.</p>
          <p>Get started by filling out the form:</p>
          <Input ref='name' type='text' label='Name' placeholder='Enter a name' />
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={event => this.handleClick(event)}>Connect</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

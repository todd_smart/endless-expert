/**
 * Simple component used to display the logged in (Local User) name - or to indicate they are not connected.
 */
import React from 'react';

export class UserInfo extends React.Component {

  static propTypes = {
    localUser : React.PropTypes.object.isRequired
  }

  /**
   * React Component Renderer
   *
   * @returns {XML}
   */
  render () {
    let element;
    if (this.props.localUser.name)  {
      element = (<div>Connected as <strong>{this.props.localUser.name}</strong></div>);
    } else {
      element = (<div>Not Connected</div>);
    }

    return element;
  }
}

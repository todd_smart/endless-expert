/**
 * The ExpertList handles presenting the data in the RemoteUser collection. It is also used to communicate an
 * invitation request to a remote user (aka Expert) by leveraging the
 * {@link /src/actions/users} inviteRemoteUser action to add the RemoteUser to the {@link /src/stores/index}.
 */
import React from 'react';
import { Panel, ListGroup, ListGroupItem, Button, Glyphicon } from 'react-bootstrap';

export class ExpertList extends React.Component {

  static propTypes = {
    remoteUsers : React.PropTypes.array.isRequired,
    inviteRemoteUser: React.PropTypes.func.isRequired,
    presence: React.PropTypes.object.isRequired
  }

  /**
   * Event handler for the click event of a local user sending an invitation to a remote user (aka Expert).
   *
   * @param {Object} remoteUser - A RemoteUser found in the to the {@link /src/stores/index}
   */
  handleClick(remoteUser) {
    this.props.inviteRemoteUser(remoteUser, this.props.presence);
  }

  /**
   * Render for the Remote Expert List that is compiled from the collection of Remote users in the
   * {@link /src/stores/index}l
   */
  renderRemoteExpertsList () {
    return (
      this.props.remoteUsers.map(user => {
        return (
          <ListGroupItem key={user.id}>
            {user.name}&nbsp;
            <Button onClick={() => this.handleClick(user)}>
              <Glyphicon glyph='glyphicon glyphicon-facetime-video'/>
            </Button>
          </ListGroupItem>
        );
      })
    );
  }

  /**
   * React Component Renderer
   *
   * @returns {XML}
   */
  render () {
    return (
      <Panel header='Experts'>
        <ListGroup>
          {this.renderRemoteExpertsList()}
        </ListGroup>
      </Panel>
    );
  }
}

/**
 * The VideoHolder handles presenting a chat once an invitation has been sent and accepted.
 *
 * The component is made aware of a trigger to start a chat by listening for an 'accept invitation' message from the
 * Presence Session that all users have connected to.
 *
 * Once a chat invitation has been accepted. This component creates a new session - "the Chat Session" - which is used
 * for further signaling and video capabilities between the inviter and invitee.
 *
 */
/* global OT */
import React from 'react';
import { Panel, Button } from 'react-bootstrap';

export class VideoHolder extends React.Component {

  static propTypes = {
    presence: React.PropTypes.object.isRequired,
    removeInvitation: React.PropTypes.func.isRequired
  }

  componentWillMount () {
    this.props.presence.session.on('signal:acceptInvitation', this.invitationAccepted, this);
  }

  // chat session used within this video holder {@link https://tokbox.com/developer/sdks/js/reference/Session.html}
  static chatSession = null
  // the publisher of the chat {@link https://tokbox.com/developer/sdks/js/reference/Publisher.html}
  static publisher = null

  static videoProperties = {
    insertMode: 'append',
    width: '100%',
    height: '100%',
    style: {
      buttonDisplayMode: 'off'
    }
  }

  /**
   * Handles the "accept invitation" signal on the Presence Session. This method then creates a Chat Session (see {@link
   * https://tokbox.com/developer/sdks/js/reference/Session.html} and initializes the WebRTC Video Chat (see {@link
   * http://www.webrtc.org/}).
   *
   * @param {Object} signalEvent - The {@link https://tokbox.com/developer/sdks/js/reference/SignalEvent.html} to
   * communicate the acceptance of a video chat request.
   */
  invitationAccepted (signalEvent) {
    const invitation = signalEvent;
    const data = JSON.parse(invitation.data);

    // setup a new chat session
    this.chatSession = OT.initSession(data.apiKey, data.sessionId);
    this.chatSession.on('sessionConnected', this.sessionConnected, this)
      .on('streamCreated', this.streamCreated, this);
    this.chatSession.on('sessionDisconnected', self.sessionDisconnected, self);

    // connect to the chat session
    this.chatSession.connect(data.token, function responseError(err) {
      if (err) {
        throw err;
      }
    });

    // initialize as the publisher of the chat and prepare to display that chat within the Publisher DOM element
    const publisherEl = document.querySelector('#publisher');
    this.publisher = OT.initPublisher(publisherEl, this.videoProperties);

    // the chat has been acccepted, so remove the invitation from the store
    this.props.removeInvitation(signalEvent);
  }

  /**
   * Once the Chat Session has been connected to, we can publish the video publisher's video into the chat
   */
  sessionConnected () {
    this.chatSession.publish(this.publisher);
  }

  /**
   * Once the Chat Video Stream has been created, we can subscribe to it and, thus, enter into the chat ourselves. Our
   * chat is then "broadcast" to the subcriber DOM element.
   *
   * @param {Object} event - The Stream Created Event {@link https://tokbox.com/developer/sdks/js/reference/StreamEvent.html}
   */
  streamCreated (event) {
    const subscriberEl = document.querySelector('#subscriber');
    this.chatSession.subscribe(event.stream, subscriberEl, this.videoProperties);
  }

  /**
   * Handle the "end chat" click by either chat party. This simply disconnects from the chat session and the app
   * components listening for that event can clean up accordingly.
   */
  handleEndChatClick () {
    this.chatSession.disconnect();
  }

  /**
   * Handles cleanup once a user has disconnected from the Chat Session (either by clicking "End Chat" or closing
   * their browser. Here, we just setup for the next chat.
   */
  sessionDisconnected () {
    this.chatSession.off();
    this.chatSession = null;
    this.publisher = null;
  }

  /**
   * Renderer for the React Component. This component includes a couple DOM elements for the Publisher and Subscriber
   * of a video chat session used to host that chat.
   *
   * @returns {XML}
   */
  render () {
    return (
      <Panel header=' '>
        <p>Invite Expert to start chatting</p>
        <div className='panel-body video-holder'>
          <div id='publisher' className='publisher'/>
          <div id='subscriber' className='subscriber'/>
          <div className='bottom-bar'>
            <Button type='button'  onClick={() => this.handleEndChatClick()} bsStyle='danger'>End Chat</Button>
          </div>
        </div>
      </Panel>
    );
  }
}

/**
 * The InvitationList handles presenting the data in the Invitation collection. It displays what (if any) outgoing and
 * what (if any) incoming chat invitations exist. It is also used to communicate the Acceptance, Declination, and
 * Cancellation of chat invitations.
 *
 * Note that there is no use of Redux Actions here. We're simply leveraging the OpenTok Signaling API in order to
 * accept, decline, an cancel invitations. See {@link https://tokbox.com/developer/sdks/js/reference/Session.html#signal}.
 */
import React from 'react';
import { Alert, Button  } from 'react-bootstrap';

export class InvitationList extends React.Component {

  static propTypes = {
    presence: React.PropTypes.object.isRequired,
    localUser: React.PropTypes.object.isRequired,
    invitations: React.PropTypes.array.isRequired
  }

  /**
   * This event handler is used for accepting, declining, and canceling invitations.
   *
   * @param {Object} invitation - The invitation to act upon.
   * @param {Boolean} accept - True if this is a click to accept. Otherwise, we know it is a cancel or decline.
   */
  handleClick (invitation, accept) {
    if (accept) {
      this.acceptInvitation(invitation);
    } else {
      this.cancelOrDeclineInvitation(invitation);
    }
  }

  /**
   * Handles the acceptance of an invitation. This method leverages the OpenTok Signaling API in order to
   * communicate to the Sender of the Invitation that the invitation has been accepted. See
   * {@link https://tokbox.com/developer/sdks/js/reference/Session.html#signal}.
   *
   * @param {Object} invitation - The invitation to act upon.
   */
  acceptInvitation (invitation) {
    const me = this;
    const signal = {
      // Construct an "accept" signal that can be sent via OpenTok
      type: 'acceptInvitation',
      to: invitation.remoteUser,
      data: JSON.stringify({
        sessionId: invitation.sessionId,
        apiKey: invitation.apiKey,
        token: invitation.token
      })
    };

    // signal the Remote User that the invititation has been accepted
    me.props.presence.session.signal(signal, function remoteResponse(remoteSignalErr) {
      if (remoteSignalErr) {
        throw remoteSignalErr;
      }
      // signal ourselves (Local User), too, so we get some reuse within the VideoHolder.invitationAccepted()
      signal.to = me.props.localUser.connection;
      me.props.presence.session.signal(signal, function localResponse(localSignalError) {
        if (localSignalError) {
          throw localSignalError;
        }
      });
    });
  }

  /**
   * Handles canceling or declining an invitation. The cancel would come from the Local User (the Inviter). The
   * decline would come from the Remote User (the Invitee). We use the OpenTok Signaling API to accomplish this
   * communication {@link https://tokbox.com/developer/sdks/js/reference/Session.html#signal}.
   *
   * @param {Object} invitation - The invitation to act upon.
   */
  cancelOrDeclineInvitation (invitation) {
    const me = this;
    // Construct a "cancel or decline" signal that can be sent via OpenTok
    const signal = {
      type: 'cancelOrDeclineInvitation',
      to: invitation.remoteUser,
      data: JSON.stringify({
        sessionId: invitation.sessionId,
        apiKey: invitation.apiKey,
        token: invitation.token
      })
    };
    this.props.presence.session.signal(signal, function remoteResponse(remoteSignalErr) {
      if (remoteSignalErr) {
        throw remoteSignalErr;
      }
      // signal ourselves (localUser), too, so we get some reuse within the App.cancelOrDeclineInvitation()
      signal.to = me.props.localUser.connection;
      me.props.presence.session.signal(signal, function localResponse(localSignalError) {
        if (localSignalError) {
          throw localSignalError;
        }
      });
    });
  }

  /**
   * Render an outgoing invitation visible to the Local User who sent them, that can be canceled by this Local User.
   *
   * @param {Object} invitation - The invitation to act upon.

   * @returns {XML}
   */
  renderOutgoingInvitation (invitation) {
    const name = JSON.parse(invitation.remoteUser.data).name;
    return (
      <Alert bsStyle='info' key={invitation.id}>
        <p>Waiting for <strong>{name}</strong> to accept your chat invitation&nbsp;
          <Button bsStyle='danger' onClick={() => this.handleClick(invitation, false)}>Cancel</Button>
        </p>
      </Alert>
    );
  }

  /**
   * Render an incoming invitation sent from the Remote User, shown here for the Local User to accept or decline.
   *
   * @param {Object} invitation - The invitation to act upon.

   * @returns {XML}
   */
  renderIncomingInvitation (invitation) {
    const name = JSON.parse(invitation.remoteUser.data).name;
    return (
      <Alert bsStyle='info' key={invitation.id}>
        <p><strong>{name}</strong> has invited you to chat&nbsp;
          <Button bsStyle='success' onClick={() => this.handleClick(invitation, true)}>Accept</Button>&nbsp;
          <Button bsStyle='danger' onClick={() => this.handleClick(invitation, false)}>Decline</Button>
        </p>
      </Alert>
    );
  }

  /**
   * Renders all invitations - inbound - invites to the local user and - outbound - invites to remote users

   * @returns {Array} The array of invitations to render.
   */
  renderInvitations () {
    const invitationMessages = [];
    this.props.invitations.map(invitation => {
      if (invitation.incoming) {
        invitationMessages.push(this.renderIncomingInvitation(invitation));
      } else {
        invitationMessages.push(this.renderOutgoingInvitation(invitation));
      }
    });
    return invitationMessages;
  }

  /**
   * React Component Renderer
   *
   * @returns {XML}
   */
  render () {
    return (
      <div>
        {this.renderInvitations()}
      </div>
    );
  }
}

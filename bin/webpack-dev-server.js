/**
 * This script was initially provided in the {@link https://github.com/davezuko/react-redux-starter-kit.}
 *
 * This script is used to startup the webpack development server on port 8080.
 */
require('babel/register');

const devServer = require('../build/webpack-dev-server'),
      config    = require('../config');

const host = config.get('webpack_host');
const port = config.get('webpack_port');
devServer.listen(port, host, function () {
  console.log(`Webpack dev server running at ${host}: ${port}`);
});

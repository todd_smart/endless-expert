/**
 * This script was initially provided in the {@link https://github.com/davezuko/react-redux-starter-kit.}
 *
 * NOTE: tsmart 11122015 - This is the ONLY change made to this script was to add the proxy config so the webpack dev
 * server can access the services being provided by the /bin/server.js node express server.
 */
import webpack          from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import config           from '../config';
import webpackConfig    from './webpack/development_hot';

const paths = config.get('utils_paths');

const server = new WebpackDevServer(webpack(webpackConfig), {
  contentBase : paths.project(config.get('dir_src')),
  hot    : true,
  quiet  : false,
  noInfo : false,
  lazy   : false,
  stats  : {
    colors : true
  },
  historyApiFallback : true,
  proxy: {
    '/services/*': {
      target: 'http://localhost:8081',
      secure: false
    }
  }  
});

export default server;

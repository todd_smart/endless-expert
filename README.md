Endless Expert: Chat with Presence Project - Phase 1
=======================
## References:

1. [The Concierge App's Endless Expert Phase 1 Notes](https://madmobile.atlassian.net/wiki/display/PROD/Phase+1+Project+Notes): 
   Outlines the scope and history of this project.

2. [OpenTok On-Demand Meeting Starter Kit](https://github.com/opentok/presencekit-php): 
   The core of this code was adapted from the OpenTok On-Demand Meeting Starter Kit. That kit 
   was written with a PHP back-end and a Backbone/Bootstrap front-end. The Endless Expert 
   application is nearly a full rewrite using [Node.js](https://nodejs.org/en/), [Express](http://expressjs.com/), [React](https://facebook.github.io/react/), and [React-Bootstrap](https://react-bootstrap.github.io/).

3. [React Redux Starter Kit](https://github.com/davezuko/react-redux-starter-kit): 
   The supporting server framework and application build system was adapted from the React 
   Redux Starter Kit. Details of its setup and usage are outlined below the application description 
   that follows these references.

## Setup

1. Requires account on [OpenTok](https://dashboard.tokbox.com/).

2. Create a new OpenTok project from the TokBox Dashboard.

3. Note your new project's API KEY and API SECRET.

4. Within that project, generate one relayed session (also using the Project Tools in the Dashboard)
   and note the generated PRESENCE SESSION ID.

5. Insure the following Environment Variables are available to your app server hosting system:

   API_KEY: [insert key]
   API_SECRET: [insert secret]
   PRESENCE_SESSION: [insert presence session]

## Quick Start

Details of the use of the project build system are available in the "React Redux Starter Kit" section of this document. 
In the meantime, start up the the Endless Expert: Chat with Presence application as follows:

1. Insure you've completed the Setup above.

2. npm run clean

3. npm run compile

4. npm run server

5. Navigate to http://localhost:8081

## Usage

1. Visit the URL mapped to the application by your web server.

2. Input a name to be identified with. You will now see a list of available users, which will start
   as empty.

3. Have another user (possibly in another window or tab) visit the same URL, and input a new user
   name. Each of the users will see one another in the Experts list.

4. In order to start a chat with an available user, click the camera icon next to the user's name. A 
   user who has sent an invitation cannot receive invitations from other users. Unavailable users
   do not have a camera icon next to their name in the Buddies list. While waiting for a response to an 
   invitation, the user can use the "cancel" button to stop the
   invitation.

5. An invited user receives an invitation to chat, which they can either accept or decline.
   If accepted, the chat begins. If declined, the invitation disappears. An invited user can continue 
   to receive invitations from other users. If the user accepts an invitation, all other invitations are automatically declined.

6. Once either of the users chooses to "end chat," both users will return to being available.

## Code and Conceptual Walkthrough

### Technologies

This application uses some of the same frameworks and libraries as the
[Learning OpenTok Web](https://github.com/toddsmart/learning-opentok-web) app. If you have not 
already gotten familiar with the code in that project, consider doing so before
continuing.

### Concepts

Each user needs to be aware of the state of each of the other users -- this is known as _presence_.
This application approaches this from a distributed point of view, which means there is no central
authority required and there's less overhead in storing each user's state. The implementation
requires a basic messaging channel where each user and the whole group can be addressed. OpenTok
includes that exact functionality for clients via the signaling API. This application uses a
**presence session**, a global session which every client connects to in order to communicate
presence information. No video streaming is done in this session.

Each client uses the presence session to build a list of the other users and keep it synchronized
as users' states change. A **remote user** is the representation of another user and its state.
This list of remote users is called the **buddies list**. When a user joins the app (adding a user
name), that client sends a signal to the presence session with details about the user. Each other
user stores the remote user's data and state.

A **local user** is the user who is using the application in the local browser.

Each users state changes based on chat invitations and ongoing chat sessions. For example, the
user's state may be unavailable, outgoingInvitePending, or chatting. The application uses the
state of each remote user, the presence information, to conduct one-to-one chats. These chats begin
as an **invitation** from one user to another. The client who is creating the invitation is in
charge of creating the new chat by requesting it from the server. The server uses the OpenTok Node
library to create a new OpenTok session for the video streaming of the chat. The **chat** contains
the OpenTok session ID, API Key, and token for the user. When the invitation is accepted, the
invited user requests the same chat from the server, and the server response will contain the same
chat information but with a unique token. Once both parties have this chat, they connect to it and
publish and subscribe to audio-video streams, much like the Hello World sample.

### Server

The server responds in a RESTful manner to a few different paths. Each path is given its own handler
in the Slim application and is individually described below:

*  `GET /presence` -- The server returns the API key and the session ID for the presence session as
   a JSON-encoded response. Notably, the token is not generated.

*  `POST /users` -- In order for a user to connect to the presence session, they must post the
   required details about themselves to this endpoint. In this case, the required details just
   include a JSON encoded `name`. The handler uses the OpenTok token's connection data feature
   to store the user name in the token. The distinction between the name and the other state
   (which is sent over the presence session) is that the name never changes, so its ideal for
   storing in the connection data. Also, note that the token is given the role `Role::SUBSRCIBER`.
   This is because no users are allowed to publish into the presence session. If you were interested
   in adding authentication for the user, you would do so in this handler.

*  `POST /chats` -- When a user chooses to invite another user to a chat, it receives its the chat's
   representation from this handler. This handler uses the OpenTok Node library to create a new
   session and return its ID along with the API key and a token. The token is unique for each
   participant in the chat. Since there is no authentication in this application, there is no
   opportunity to perform authorization in the chat. If there was, you could use this handler to
   create a record for the chat in a database; then when the invited user requests the chat, the
   handler could authorize data  to make sure the requesting user is the invited user for that chat.

*  `GET /chats?sessionId=[sessionId]` -- When the invited user accepts an invitation, it also needs
   the details required to connect to the chat. Since there is no authentication in this
   application, and there are no database records for each chat that is created, the invited user is
   expected to know the sessionId of the chat they want to join already and send it as a query
   string parameter. The handler then returns the same details required to connect to the chat
   (session ID, API Key, token) but with another unique token.

### Client

The client JavaScript code is divided into separate files that each define a React or Redux class used by
the app. These classes are described in-line using JSDoc.

React Redux Starter Kit
=======================

[![Join the chat at https://gitter.im/davezuko/react-redux-starter-kit](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/davezuko/react-redux-starter-kit?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Build Status](https://travis-ci.org/davezuko/react-redux-starter-kit.svg?branch=master)](https://travis-ci.org/davezuko/react-redux-starter-kit?branch=master)
[![dependencies](https://david-dm.org/davezuko/react-redux-starter-kit.svg)](https://david-dm.org/davezuko/react-redux-starter-kit)

Starter kit to get you up and running with a bunch of awesome new front-end technologies, all on top of a configurable, feature-rich Webpack build system that's already setup to provide unit testing, linting, hot reloading, sass imports with CSS extraction, and a whole lot more. Check out the full feature list below!

Redux, React-Router, and React are constantly releasing new API changes. If you'd like to help keep this boilerplate up to date, please contribute or create a new issue if you think this starter kit is missing something!

**Where'd the server go?**. This starter kit used to come packaged with a Koa server to perform basic server-side rendering. However, despite that, universal rendering remained secondary to the goal of this starter kit, so it made more sense to remove that aspect; after all, do one thing and do it well.

Table of Contents
-----------------
1. [Requirements](#requirements)
1. [Features](#features)
1. [Getting Started](#getting-started)
1. [Usage](#usage)
1. [Structure](#structure)
1. [Webpack](#webpack)
1. [Styles](#styles)
1. [Testing](#testing)
1. [Utilities](#utilities)
1. [Troubleshooting](#troubleshooting)

Requirements
------------

Node `^4.0.0`

Features
--------

* [React](https://github.com/facebook/react) (`^0.14.0`)
  * Includes react-addons-test-utils (`^0.14.0`)
* [React-Router](https://github.com/rackt/react-router) (`1.0.0-rc1`)
* [Redux](https://github.com/gaearon/redux) (`^3.0.0`)
  * redux-router (`^1.0.0-beta3`)
  * react-redux (`^4.0.0`)
  * redux-devtools
    * use `npm run dev:nw` to display in a separate window.
  * redux-thunk middleware
* [Karma](https://github.com/karma-runner/karma)
  * Mocha w/ Chai and Sinon-Chai
  * PhantomJS
* [Babel](https://github.com/babel/babel)
  * `react-transform-hmr` for hot reloading
  * `react-transform-catch-errors` with `redbox-react` for more visible error reporting
  * Uses babel runtime rather than inline transformations
* [Webpack](https://github.com/webpack/webpack)
  * Splits app code from vendor dependencies
  * webpack-dev-server
  * sass-loader with CSS extraction
  * eslint-loader
    * Uses [Airbnb's eslint config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) (with some softened rules)
    * Configured to fail production builds on error
  * Pre-configured folder aliases and globals

Getting Started
---------------

Just clone the repo and install the necessary node modules:

```shell
$ git clone https://github.com/davezuko/react-redux-starter-kit.git ReduxStarterApp
$ cd ReduxStarterApp
$ npm install                   # Install Node modules listed in ./package.json (may take a while the first time)
$ npm start                     # Compile and launch
```

Usage
-----

#### `npm start`
Runs the webpack build system with webpack-dev-server (by default found at `localhost:3000`).

#### `npm run dev:nw`
**Currently unavailable with redux-devtools @ ^3.0.0**. Same as `npm run start` but opens the debug tools in a new window.

**Note:** you'll need to allow popups in Chrome, or you'll see an error: [issue 110](https://github.com/davezuko/react-redux-starter-kit/issues/110)

#### `npm run dev:no-debug`
Same as `npm run start` but disables devtools.

#### `npm run compile`
Runs the Webpack build system with your current NODE_ENV and compiles the application to disk (`~/dist`). Production builds will fail on eslint errors (but not on warnings).

#### `npm run test`
Runs unit tests with Karma.

#### `npm run test:dev`
Same as `npm run test`, but will watch for changes and re-run tests.

#### `npm run deploy`
Helper script to run tests and then, on success, compile your application.

### Configuration

Basic project configuration can be found in `~/config/index.js`. Here you'll be able to redefine your src and dist directories, as well as tweak what ports Webpack and WebpackDevServer run on.

Structure
---------

The folder structure provided is only meant to serve as a guide, it is by no means prescriptive. It is something that has worked very well for me and my team, but use only what makes sense to you.

```
.
├── bin                      # Build/Start scripts
├── build                    # All build-related configuration
│   ├── webpack              # Environment-specific configuration files for Webpack
├── config                   # Project configuration settings
└── src                      # Application source code
    ├── components           # Generic React Components (generally Dumb components)
    ├── containers           # Components that provide context (e.g. Redux Providers)
    ├── layouts              # Components that dictate major page structure
    ├── reducers             # Redux reducers
    ├── routes               # Application route definitions
    ├── stores               # Redux store configuration
    ├── utils                # Generic utilities
    ├── views                # Components that live at a route
    └── index.js             # Application bootstrap and rendering
```

### Components vs. Views vs. Layouts

**TL;DR:** They're all components.

This distinction may not be important for you, but as an explanation: A **Layout** is something that describes an entire page structure, such as a fixed navigation, viewport, sidebar, and footer. Most applications will probably only have one layout, but keeping these components separate makes their intent clear. **Views** are components that live at routes, and are generally rendered within a **Layout**. What this ends up meaning is that, with this structure, nearly everything inside of **Components** ends up being a dumb component.

Webpack
-------

### Configuration
The webpack compiler configuration is located in `~/build/webpack`. When the webpack dev server runs, only the client compiler will be used. When webpack itself is run to compile to disk, both the client and server configurations will be used. Settings that are bundle agnostic should be defined in `~/config/index.js` and imported where needed.

### Vendor Bundle
You can redefine which packages to treat as vendor dependencies by editing `vendor_dependencies` in `~/config/index.js`. These default to:

```js
[
  'history',
  'react',
  'react-redux',
  'react-router',
  'redux-router',
  'redux'
]
```

### Aliases
As mentioned in features, the default Webpack configuration provides some globals and aliases to make your life easier. These can be used as such:

```js
import MyComponent from '../../components/my-component'; // without alias
import MyComponent from 'components/my-component'; // with alias

  // Available aliases:
  actions     => '~/src/actions'
  components  => '~/src/components'
  constants   => '~/src/constants'
  containers  => '~/src/containers'
  layouts     => '~/src/layouts'
  reducers    => '~/src/reducers'
  routes      => '~/src/routes'
  services    => '~/src/services'
  styles      => '~/src/styles'
  utils       => '~/src/utils'
  views       => '~/src/views'
```

### Globals

#### `__DEV__`
True when `process.env.NODE_ENV` is `development`

#### `__PROD__`
True when `process.env.NODE_ENV` is `production`

#### `__DEBUG__`
True when the compiler is run with `--debug` (any environment).

Styles
------

All `.scss` imports will be run through the sass-loader, extracted during production builds, and ignored during server builds. If you're requiring styles from a base styles directory (useful for generic, app-wide styles) in your JS, you can make use of the `styles` alias, e.g.:

```js
// ~/src/components/some/nested/component/index.jsx
import `styles/core.scss`;
```

Furthermore, this `styles` directory is aliased for sass imports, which further eliminates manual directory traversing. An example nested `.scss` file:

```scss
// current path: ~/src/styles/some/nested/style.scss
// what used to be this:
@import '../../base';

// can now be this:
@import 'base';
```

Testing
-------

To add a unit test, simply create `.spec.js` file anywhere in `~/src`. The entry point for Karma uses webpack's custom require to load all these files, and both Mocha and Chai will be available to you within your test without the need to import them.

Utilities
---------

This boilerplate comes with two simple utilities (thanks to [StevenLangbroek](https://github.com/StevenLangbroek)) to help speed up your Redux development process. In `~/client/utils` you'll find exports for `createConstants` and `createReducer`. The former is pretty much an even lazier `keyMirror`, so if you _really_ hate typing out those constants you may want to give it a shot. Check it out:

```js
import { createConstants } from 'utils';

export default createConstants(
  'TODO_CREATE',
  'TODO_DESTROY',
  'TODO_TOGGLE_COMPLETE'
);
```

The other utility, `create-reducer`, is designed to expedite creating reducers when they're defined via an object map rather than switch statements. As an example, what once looked like this:

```js
import { TODO_CREATE } from 'constants/todo';

const initialState = [];
const handlers = {
  [TODO_CREATE] : (state, payload) => { ... }
};

export default function todo (state = initialState, action) {
  const handler = handlers[action.type];

  return handler ? handler(state, action.payload) : state;
}
```

Can now look like this:

```js
import { TODO_CREATE } from 'constants/todo';
import { createReducer } from 'utils';

const initialState = [];

export default createReducer(initialState, {
  [TODO_CREATE] : (state, payload) => { ... }
});
```

Troubleshooting
---------------

Nothing yet. Having an issue? Report it and I'll get to it as soon as possible!
